# Ansible role vault-secret-pull

This role automates the secure fetching of secrets from Vault, ensuring the target machine always has the latest secrets available.
